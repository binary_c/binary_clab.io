#!/usr/bin/env python3

import ads
import re
import sys
from urlextract import URLExtract

############################################################
#
# Script to make the papers list for binary_c
#
# run:
#
# ./process_papers.py  > Papers.astro
#
############################################################

def bibcode(url):
    # return bibcode of a url
    chunks = url.split('/')
    for i,chunk in enumerate(chunks):
        if chunk == 'abs':
            return chunks[i+1]

def fixhtml(string):
    # fix mistakes in HTML
    string = string.replace('SUP','sup')
    return string

def fixauthors(authors):
    # reformat authors
    newauthors = []
    authors = paper.author
    for author in authors:
        chunks = author.split(',')
        surname = chunks.pop(0)
        initials = ' '.join(chunks).replace('. ','.')
        newauthors.append(fixhtml(f"{initials} {surname}".strip()))
    return newauthors

############################################################

# make list of bibcodes
f = open('papers.dat','r')
bibcodes = []
extractor = URLExtract()
for line in f:
#testdata = ["https://ui.adsabs.harvard.edu/abs/2023MNRAS.524.4315H/abstract\n","https://ui.adsabs.harvard.edu/abs/2023JOSS....8.4642H/abstract\n"];for line in testdata:
    us = extractor.find_urls(line)
    if us:
        for u in us:
            u = u.replace('%26','&')
            bibcodes += [(bibcode(u),u)]


print("---\n---\n\n");

# parse bibcodes
data = {}
count=0
years = { 'min' : 9999, 'max' : 0 }
for (bibcode,url) in bibcodes:
    l = list(ads.SearchQuery(identifier=bibcode))
    if len(l)==0:
        l = list(ads.SearchQuery(alternate_bibcode=bibcode))
    if len(l) == 0:
        print(f"No data for bibcode {bibcode}")
        sys.exit()

    year = int(l[0].year)
    years['min'] = min(years['min'], year)
    years['max'] = max(years['max'], year)
    if not year in data:
        data[year] = {}
    data[year][bibcode] = (l[0],url,l[0].citation_count)

# loop over years, outputting if available
n = 0
n_citations = 0
for year in range(years['max'],years['min']-1,-1):
    if year in data:
        print(f"<br><b><h1>{year}</h1></b><br>")
        for (bibcode,(paper,url,citation_count)) in data[year].items():
            title = fixhtml(' '.join(paper.title))
            authors = fixauthors(paper.author)
            if citation_count > 0:
                n_citations += citation_count
                this_citations = f" {citation_count} citations"
            else:
                this_citations = ''
            print(f"<li><b>{title}</b><br>{', '.join(authors)}<br> <a href=\"{url}\">ADS</a>{this_citations}</li>")
            n += 1

print(f"Have output {n} papers in the list, with {n_citations} citations.",
      file=sys.stderr)
